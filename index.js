const toggle = document.getElementById("toggle");
toggle.addEventListener("change",()=>{
    if(toggle.checked){
        document.getElementById("price1").textContent = 19.99;
        document.getElementById("price2").textContent = 24.99;
        document.getElementById("price3").textContent = 39.99;

    } else {
        document.getElementById("price1").textContent = 199.99;
        document.getElementById("price2").textContent = 249.99;
        document.getElementById("price3").textContent = 399.99;
    
    }
})
